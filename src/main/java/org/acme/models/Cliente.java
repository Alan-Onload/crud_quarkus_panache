/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author alan
 */
@Entity
public class Cliente extends PanacheEntity{
        
    public String data;
    @NotBlank(message = "o campo nome não pode ser nulo")
    public String nome;
    @NotBlank(message = "o campo telefone não pode ser nulo")
    public String telefone;
    @NotBlank(message = "o campo email não pode ser nulo")
    public String email;
    @NotBlank(message = "o campo cep não pode ser nulo")
    public String cep;
}
