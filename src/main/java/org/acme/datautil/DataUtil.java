/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.datautil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;


/**
 *
 * @author alan
 */

public class DataUtil {
    
    public String dataHoje(){
    DateFormat formate = new SimpleDateFormat("dd/MM/yyyy");
    return formate.format(new Date());
    }
    
}
