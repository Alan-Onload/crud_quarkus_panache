/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.acme.controller;

import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.acme.datautil.DataUtil;
import org.acme.models.Cliente;

@Path("/cliente")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClienteController {
    
    
    private DataUtil dt;
    @Inject
    Validator validator;
        
    @GET
    @Path("/")
    public List<Cliente> findAll() {
        return Cliente.listAll();
    }
    
    @GET
    @Path("/{id}")
    public Cliente findById(@PathParam("id") Long id) {
        return Cliente.findById(id);
    }
    
    @POST
    @Transactional
    @Path("/")
    public Cliente save(@Valid Cliente cliente) {
        Set<ConstraintViolation<Cliente>> violations = validator.validate(cliente);
        Cliente cl = new Cliente();
        if(violations.isEmpty()){
        dt = new DataUtil();
        cl.data = dt.dataHoje();
        cl.nome = cliente.nome;
        cl.telefone = cliente.telefone;
        cl.email = cliente.email;
        cl.cep = cliente.cep;
        cl.persist();
        return cl;
        }else{
         return new Cliente();
        }

    }
    
    @PUT
    @Transactional
    @Path("/{id}")
    public Cliente update(@PathParam("id") Long id, @Valid Cliente cliente) {
        Cliente cl = Cliente.findById(id);
        cl.nome = cliente.nome;
        cl.telefone = cliente.telefone;
        cl.email = cliente.email;
        cl.cep = cliente.cep;
        cl.persist();
        return cl;

    }
    
    @DELETE
    @Transactional
    @Path("/{id}")
    public Cliente delete(@PathParam("id") Long id) {
        Cliente cl = Cliente.findById(id);
        cl.delete();
        return cl;

    }
    
}
